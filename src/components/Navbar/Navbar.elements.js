import styled from "styled-components";


export const Container = styled.div`
  width: 100%;
  height: 120px;
  background-color: #1c1c1c; ;
`;

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1300px;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: auto;
`;

export const LogoContainer = styled.div`
  margin-left: 0.5rem;
  display: flex;
  align-items: center;
  font-size: 1.7rem;
  font-family: 'Dancing Script';

  p {
    &:nth-child(2) {
    font-size: 2.2rem;
    color: #fff;
    margin-bottom: 2.5rem!important;
    
    margin-top: 0rem!important;
    }

    &:nth-child(3) {
    font-size: 2.3rem;
    color: #5e2129;
    margin-bottom: 2.5rem!important;
    margin-top: 0rem!important;
    
    }
  }

  svg {
    fill: #5e2129;
    margin-right: 0.5rem;
    margin-bottom: 1.8rem;
    margin-top: -1.5rem!important;
    
    
  }
`;

export const Menu = styled.ul`
  height: 100%;
  display: flex;
  justify-content: space-between;
  list-style: none;
  margin-top: 1rem;
  }

  @media screen and (max-width: 960px) {
    background-color: #23394d;
    position: absolute;
    top: 70px;
    left: ${({ open }) => (open ? "0" : "-100%")}; //Import
    width: 100%;
    height: 90vh;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    transition: 0.5s all ease;
  }
`;

export const MenuItem = styled.li`
  height: 50%;

  @media screen and (max-width: 960px) {
    width: 100%;
    height: 70px;
    display: flex;
    justify-content: ce;
    align-items: center;
  }
`;

export const MenuItemLink = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 0.5rem 2.5rem;
  color: #5e2129;
  font-family: 'Dancing Script';
  font-size: 2rem;
  font-weight: 600;
  cursor: pointer;
  transition: 0.5s all ease;

  &:hover {
    background-color: #5e2129;
    color: #fff;
    box-shadow: 0 4px 16px #fff;
    transition: all 0.2s ease;

    div {
      svg {
        fill: #23394d;
      }
    }
  }

  div {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;

    svg {
      display: none;
      fill: #e0792a;
      margin-right: 0.5rem;
    }
  }

  @media screen and (max-width: 960px) {
    width: 100%;

    div {
      width: 30%;
      justify-content: left;

      svg {
        display: flex;
      }
    }
  }

  @media screen and (max-width: 880px) {
    div {
      width: 40%;
      justify-content: left;

      svg {
        display: flex;
      }
    }
  }

  @media screen and (max-width: 500px) {
    div {
      width: 60%;
      justify-content: left;

      svg {
        display: flex;
      }
    }
  }

  @media screen and (max-width: 260px) {
    div {
      width: 100%;
      justify-content: left;

      svg {
        display: flex;
      }
    }
  }
`;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 960px) {
    display: flex;
    align-items: center;
    cursor: pointer;

    svg {
      fill: #e07924;
      margin-right: 0.5rem;
    }
  }
`;
