import React from 'react'
import Item from "./Item"
const ItemList = ({productos}) => {
  return (
    <>
    {
        productos.map((item, i) =>
            <Item key= {item.id} imagen={item.imagen} nombre={item.nombre} descripcion={item.descripcion} precio={item.precio} />
        )
    }
    </>
  )
}

export default ItemList