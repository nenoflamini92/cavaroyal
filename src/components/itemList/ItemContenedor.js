import React, { useEffect, useState } from 'react'
import ItemList from './ItemList'




const ItemContenedor = () => {
    const [productos, setProductos] = useState([])

    useEffect(() => {
        let productosLista = ([
            {id:1,
                nombre: "RUTINI CABERNET FRANC MALBEC 750ml",
                descripcion: "Rojo intenso, con matices azulados. En nariz, se presenta frutado, con notas de ciruela, vainilla y anís",
                precio : "2.500"
            },
            {
                id:2,
                nombre: "NORTON LOTE LA COLONIA MALBEC 750ml",
                descripcion: "Color Rojo intenso con aromas Dulce, compleja, con notas a frambuesa y frutos rojos maduros dejando un sabor en boca ",
                precio : "$3.660"
            },
            {
                id:3,
                nombre: "LOS NOBLES MALBEC VERDOT 750ml",
                descripcion: "Tinto de color púrpura profundo, de aromas equilibrados que recuerdan a frutos negros, moras, grosellas y ciruelas maduras",
                precio : "$5.980"
            },
            {
                id:4,
                nombre: "LUIGI BOSCA ICONO 750ml",
                descripcion: "Tinto de color rojo granate con tintes negros profundos. En nariz es amplio, mostrando arándanos, cerezas y notas florales.",
                precio : "$15.130"
            },
            {
                id:5,
                nombre: "GRAN ALMA NEGRA BLEND 1500ml",
                descripcion: "De cuerpo medio y muy terso, suave de gran equilibrio armónico entre sus frutas. ",
                precio : "$8.290"
            },
            {
                id:6,
                nombre: "DIEGO ROSSO MALBEC 750ml",
                descripcion: "Producidos con uvas provenientes de su viñedo en San Pablo, Tupungato, en un micro-terroir único ",
                precio : "$3.280"
            },
            {
                id:7,
                nombre: "GRAN MEDALLA MALBEC 1500ml",
                descripcion: "De color rojo púrpura, este vino presenta un fino bouquet con aromas de cerezas negras y ciruelas",
                precio : "$7.850"
            },
            {
                id:8,
                nombre: "ANAIA GRAND ASSEMBLAGE 750ml",
                descripcion: "Corte de micro fermentaciones en barricas y rolls fermentors de 500 y 600 litros de roble francés de primer uso",
                precio : "$3.510"
            },
            {
                id:9,
                nombre: "COCODRILO RED BLEND 1 1500ml",
                descripcion: "Rojo intenso con reflejos violáceos, este blend cautiva con su gran concentración, cuerpo y estructura.",
                precio : "$12.480"
            }]);

            const miPromesa = new Promise((res, rej) => {
                setTimeout(() => {
                    res(productosLista);
                }, 2000);
            });
            miPromesa.then((productos) =>{
                setProductos(productos);
            });
    }, [])


  return (
    <><ItemList productos={productos} /></>
  )
}

export default ItemContenedor