import React from 'react'
import Card from 'react-bootstrap/Card';
import ItemCount from '../itemCount/ItemCount';
import './vinos.css'




const onAdd = (cantidad) => {
  console.log(`Se agrego ${cantidad}`);
}
function Item({imagen, nombre, descripcion, precio}) {

  
  return (
    
        
    <Card style={{ width: '17.8rem', height: '31rem', background: '#5e2129', margin: 'auto', marginTop: '5rem'}}>
      <Card.Img variant="top" src={imagen} style={{height: '13rem', width: '11.4rem', margin: 'auto'}}/>
      <Card.Body>
        <Card.Title style={{color: 'white', fontSize: '25px', textAlign: 'center'}}>{nombre}</Card.Title>
        <Card.Text style={{color: 'white', fontSize: '20px'}}>{descripcion}</Card.Text>
        <Card.Text style={{color: 'white', fontSize: '20px'}}>{precio}</Card.Text>

        <ItemCount stock = {5} initial = {1} onAdd={onAdd}/>
      </Card.Body>
    </Card>
  );
}

export default Item;