import React from 'react'

import { FaShoppingCart } from "react-icons/fa";

const CartWidget = () => {
  return (
    <>
    
        <FaShoppingCart style={{ width: '50px', color: 'white', marginTop: '1rem', cursor: 'pointer'}}/>
    
    </>

  )
}

export default CartWidget