import React from 'react'
import Card from 'react-bootstrap/Card';

function BasicExample({titulo,imagen,descripcion}) {
  return (
        
    <Card style={{ width: '18rem', background: '#5e2129', margin: 5}}>
      <Card.Img variant="top" src={imagen}/>
      <Card.Body>
        <Card.Title style={{color: 'white', fontSize: '35px', textAlign: 'center'}}>{titulo}</Card.Title>
        <Card.Text style={{color: 'white', fontSize: '20px'}}>{descripcion}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default BasicExample;