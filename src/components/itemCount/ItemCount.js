import React, { useState } from 'react'

import Button from 'react-bootstrap/Button';

const ItemCount = ({stock, initial, onAdd}) => {
    
    const[count,setCount] = useState(initial); 

    const sumar = ()=>{
        if(stock > count){
            setCount(count + 1)
        }
    }

    const restar = () => {
        if( count > 1){
            setCount(count - 1)
        }
    }
    const AgregarCantidad = () => {
        onAdd(count)
    }

    return (
        <>
        <div styled={{marginLeft: '10vh', display: 'flex'}}>
            <Button onClick={restar} variant="primary" id="center" size="lg" active styled={{width: "100%", height: '100px', fontSize: '1px', aligItems: "center", justifyContent: "center"}}>-</Button>
            
            <label style={{justifyContent: 'center', textAlign: 'center', color: 'white', fontSize: '20px', marginLeft: '3px'}}> {count} </label>

            
            
            <Button onClick={sumar} variant="primary" id="center" size="lg" active styled={{width: "100%", height: '100px', fontSize: '1px', aligItems: "center", justifyContent: "center"}}>+</Button>

        </div>

        <Button onClick={AgregarCantidad} class="btn" variant="primary" id="center" size="lg" active styled={{width: "100%", height: '100px', fontSize: '1px', aligItems: "center", justifyContent: "center"}} >Añadir al carrito</Button>{' '}
        
                
        </>
    )}

    export default ItemCount;
