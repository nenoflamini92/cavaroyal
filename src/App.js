import Navbar from "./components/Navbar/Navbar";
import Card from "./components/Card/Card"
import ItemContenedor from "./components/itemList/ItemContenedor";
import "./App.css"



const image1 = require("./assets/vinos2.jpg");
const image2 = require("./assets/espumantes.jpg");
const image3 = require("./assets/cervezas.jpg");
const image4 = require("./assets/destilados.jpg");





function App() {
  return (
    <>
      <Navbar />
      <div style={{display: "flex"}}>
      <Card titulo = "Vinos" imagen={image1} descripcion="Blancos, Tintos, Rosados"/>
      <Card titulo = "Espumantes" imagen={image2} descripcion="Brut Nature, Brut, Moscato, Dulces"/>
      <Card titulo = "Cervezas" imagen={image3} descripcion="Rubias, Negroas, Lager, Alemanas"/>
      <Card titulo = "Destilados" imagen={image4} descripcion="Gin, Whisky, Vodka, Ron, Tequila, Ginebra"/>  
      </div>
      <div>

      <ItemContenedor />
      </div>
      
    </>
  );
}

export default App;
